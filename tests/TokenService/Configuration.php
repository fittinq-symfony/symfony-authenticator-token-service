<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\TokenService;

use Firebase\JWT\JWT;
use Fittinq\Symfony\Authenticator\TokenService\TokenService;
use stdClass;
use Test\Fittinq\Symfony\Mock\Cache\CacheMock;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;

class Configuration
{
    private HttpClientMock $httpClient;
    private CacheMock $cache;
    private LoggerMock $logger;
    private ?TokenService $tokenService = null;

    public function __construct()
    {
        $this->httpClient = new HttpClientMock();
        $this->cache = new CacheMock();
        $this->logger = new LoggerMock();
    }

    public function configure(string $url, string $username, string $password): TokenService
    {
        if (!$this->tokenService) {
            $this->tokenService = new TokenService(
                $this->httpClient,
                $this->cache,
                $this->logger,
                $url,
                $username,
                $password
            );
        }

        return $this->tokenService;
    }

    public function getHttpClient(): HttpClientMock
    {
        return $this->httpClient;
    }

    /**
     * @return CacheMock
     */
    public function getCache(): CacheMock
    {
        return $this->cache;
    }

    /**
     * @return LoggerMock
     */
    public function getLogger(): LoggerMock
    {
        return $this->logger;
    }

    public function createToken(bool $isExpired = false): string
    {
        $data = ["expires_at" => $isExpired ? time() - 1000 : time() + 1000];
        return JWT::encode($data, file_get_contents("tests/jwt.key"), 'RS256');
    }
}