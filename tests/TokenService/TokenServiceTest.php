<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\TokenService;

use DateTime;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Fittinq\Symfony\Authenticator\TokenService\TokenService;
use PHPUnit\Framework\TestCase;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Test\Fittinq\Symfony\Mock\Cache\CacheMock;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;

class TokenServiceTest extends TestCase
{
    private HttpClientMock $httpClient;
    private CacheMock $cache;
    private TokenService $tokenService;
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = 'https://dev.auth.hip.fittinq.com';
        $this->configuration = new Configuration();
        $this->httpClient = $this->configuration->getHttpClient();
        $this->cache = $this->configuration->getCache();
        $this->tokenService = $this->configuration->configure($this->url, 'user', 'test');
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_returnTokenFromAuthenticatorIfTokenIsNotInCache()
    {
        $token = $this->configuration->createToken();
        $this->cache->delete($this->url . 'jwt');
        $this->httpClient->setUpResponse(new ResponseMock(Response::HTTP_OK, [], $token));

        $this->assertEquals($token, $this->tokenService->getToken());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_returnTokenFromCacheIfValid()
    {
        $token = $this->configuration->createToken();
        $this->cache->setUpCache($this->url . 'jwt', $token, new DateTime('now +1 day'));
        $this->httpClient->setUpResponse(new ResponseMock(Response::HTTP_OK, [], 'should_not_be_used'));

        $this->assertEquals($token, $this->tokenService->getToken());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_returnTokenFromAuthenticatorIfTokenIsExpired()
    {
        $expiredToken = $this->configuration->createToken(true);
        $token = $this->configuration->createToken();
        $this->cache->setUpCache($this->url . 'jwt', $expiredToken, new DateTime('now -2 day'));
        $this->httpClient->setUpResponse(new ResponseMock(Response::HTTP_OK, [], $token));

        $this->assertEquals($token, $this->tokenService->getToken());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_setCacheExpiryToExpiryOfTheJWTPayload()
    {
        $token = $this->configuration->createToken();
        $expiresAt = JWT::decode($token, new Key(file_get_contents("tests/jwt.key.pub"), 'RS256'))->expires_at;
        $this->cache->delete($this->url . 'jwt');
        $this->httpClient->setUpResponse(new ResponseMock(Response::HTTP_OK, [], $token));

        $this->tokenService->getToken();

        $cacheItem = $this->cache->getItem($this->url . 'jwt');
        $cacheItem->expectToExpireAt($expiresAt);
    }
}