<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\TokenService;

use Fittinq\Symfony\Authenticator\Exception\AuthenticationFailedException;
use Fittinq\Symfony\Authenticator\TokenService\TokenService;
use PHPUnit\Framework\TestCase;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LogLevel;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpFoundation\Response;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;
use Throwable;

class AuthenticationExceptionTest extends TestCase
{
    private HttpClientMock $httpClient;
    private TokenService $tokenService;
    private LoggerMock $logger;

    protected function setUp(): void
    {
        parent::setUp();

        $configuration = new Configuration();
        $this->httpClient = $configuration->getHttpClient();
        $this->logger = $configuration->getLogger();
        $this->tokenService = $configuration->configure('https://dev.auth.hip.fittinq.com', 'user', 'test');
    }

    /**
     * @dataProvider getErrorStatusCodes
     */
    public function test_throwExceptionWhenRequestFailed(int $httpStatusCode)
    {
        $this->httpClient->setUpResponse(new ResponseMock($httpStatusCode));

        $this->expectException(AuthenticationFailedException::class);

        $this->tokenService->getToken();

        $this->logger->expectToHaveLoggedLast(LogLevel::ERROR, ClientException::class . ": could not fetch token" );
    }

    /**
     * @dataProvider getErrorStatusCodes
     */
    public function test_logExceptionWhenRequestFailed(int $httpStatusCode, string $exceptionClassName)
    {
        $this->httpClient->setUpResponse(new ResponseMock($httpStatusCode));

        $this->getTokenWithoutThrowingException();

        $this->logger->expectToHaveLoggedLast(LogLevel::EMERGENCY,"{$exceptionClassName}: could not fetch token" );
    }

    public function getErrorStatusCodes(): array
    {
        return [
            [Response::HTTP_INTERNAL_SERVER_ERROR, ServerException::class],
            [Response::HTTP_UNAUTHORIZED, ClientException::class]
        ];
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_throwExceptionWhenServerException()
    {
        $this->httpClient->setUpException(new TransportException());

        $this->expectException(AuthenticationFailedException::class);

        $this->tokenService->getToken();
    }

    public function test_logExceptionWhenServerException()
    {
        $this->httpClient->setUpException(new TransportException());

        $this->getTokenWithoutThrowingException();

        $this->logger->expectToHaveLoggedLast(LogLevel::EMERGENCY,TransportException::class . ": could not fetch token" );
    }

    private function getTokenWithoutThrowingException(): void
    {
        try {
            $this->tokenService->getToken();
        } catch (Throwable) {
        }
    }
}