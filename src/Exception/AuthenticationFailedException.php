<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\Exception;

use RuntimeException;

class AuthenticationFailedException extends RuntimeException
{

}