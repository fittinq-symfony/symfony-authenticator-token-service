<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyAuthenticatorTokenServiceBundle extends Bundle
{
}