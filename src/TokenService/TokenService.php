<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\TokenService;

use DateTime;
use Fittinq\Symfony\Authenticator\Exception\AuthenticationFailedException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

class TokenService
{
    private HttpClientInterface $httpClient;
    private CacheInterface $cache;
    private TokenValidator $tokenValidator;
    private LoggerInterface $logger;
    private string $url;
    private string $username;
    private string $password;

    public function __construct(
        HttpClientInterface $httpClient,
        CacheInterface $cache,
        LoggerInterface $logger,
        string $url,
        string $username,
        string $password
    )
    {
        $this->httpClient = $httpClient;
        $this->cache = $cache;
        $this->tokenValidator = new TokenValidator();
        $this->logger = $logger;
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @throws AuthenticationFailedException
     * @throws InvalidArgumentException
     */
    public function getToken()
    {
        $token = $this->cache->get($this->url . 'jwt', function (ItemInterface $item) {
            $token = $this->fetchToken();
            $this->setExpiresAtOfCacheToExpiresAtOfJWTPayload($token, $item);
            $this->tokenValidator->validateToken($token);
            return $token;
        });

        if ($this->tokenIsExpired($token)) {
            $this->cache->delete($this->url . "jwt");
            return $this->getToken();
        }

        return $token;
    }

    /**
     * @throws AuthenticationFailedException
     */
    public function fetchToken(): string
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                "{$this->url}/api/v1/authenticate",
                ['auth_basic' => [$this->username, $this->password]]
            );

            return $response->getContent();
        }catch (Throwable $e) {
            $this->logger->emergency(get_class($e) . ": could not fetch token");
            throw new AuthenticationFailedException();
        }
    }

    private function setExpiresAtOfCacheToExpiresAtOfJWTPayload(string $token, ItemInterface $item)
    {
        $parts = explode('.', $token);

        if (count($parts) !== 3) {
            throw new AuthenticationFailedException();
        }

        $payload = json_decode(base64_decode($parts[1]));

        $item->expiresAt(DateTime::createFromFormat('U', (string) $payload->expires_at));
    }

    private function tokenIsExpired(string $token): bool
    {
        $parts = explode('.', $token);
        $payload = json_decode(base64_decode($parts[1]));

        if ($payload->expires_at < time()) {
            return true;
        }

        return false;
    }
}
