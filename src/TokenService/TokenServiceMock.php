<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\TokenService;

use Throwable;

class TokenServiceMock extends TokenService
{
    protected string $token = '';
    private ?Throwable $exception = null;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
    }

    /**
     * @throws Throwable
     */
    public function getToken(): string
    {
        if ($this->exception) {
            throw $this->exception;
        }

        return $this->token;
    }

    public function setToken(string $token)
    {
        $this->token = $token;
    }

    public function setException(Throwable $exception)
    {
        $this->exception =  $exception;
    }
}