<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\TokenService;

use Fittinq\Symfony\Authenticator\Exception\AuthenticationFailedException;

class TokenValidator
{
    public function validateToken($token)
    {
        $parts = explode('.', $token);

        if (count($parts) !== 3) {
            throw new AuthenticationFailedException();
        }

        $payload = json_decode(base64_decode($parts[1]));

        if (
            empty($payload)
            || ! isset($payload->expires_at)
        ) {
            throw new AuthenticationFailedException();
        }

    }
}