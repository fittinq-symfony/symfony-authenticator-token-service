# Symfony authenticator
Use symfony authenticator to obtain a valid token to authenticate a service.

## Install via composer
```bash
composer require fittinq\symfony-authenticator-token-service
```
## Configure bundle
```php
# config/bundles.php
<?php

return [
    // ...
    Fittinq\Symfony\Authenticator\SymfonyAuthenticatorTokenServiceBundle::class => ['all' => true],
];

```
#.env
Configure the following .env.local settings
```ini
AUTHENTICATOR_HOST_URL=authenticator
AUTHENTICATOR_USERNAME=jeroen
AUTHENTICATOR_PASSWORD=jeroen
```

